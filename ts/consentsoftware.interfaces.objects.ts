import * as plugins from './interfaces.plugins.js';

export type TCookieLevel = 'functional' | 'analytics' | 'marketing' | 'all';

/**
 * a consenttuple represents a code snippet that can be run in accordance with a cookie level
 */
export interface IConsentTuple {
  name: string;
  description: string;
  level: TCookieLevel;
  script: string | ((dataArg: any, consentLevels: TCookieLevel[]) => Promise<void>);
  scriptExecutionDataArg?: any;
}

/**
 * DomainSettings are used to store all information about a domain serverside
 */
export interface IDomainSettings {
  id: string;
  domainName: string;
  googleAnalyticsUniversalId: string;
  googleAnalytics4Id: string;
  fullStoryId: string;
  customTuples: IConsentTuple[];
}

/**
 * the webclient setting is a setting that can be stored on the client
 */
export interface IWebclientSettings {
  acceptanceTimestamp: number;
  acceptedCookieLevels: TCookieLevel[];
}
