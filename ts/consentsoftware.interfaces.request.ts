import * as plugins from './interfaces.plugins.js';

import { IConsentTuple } from './consentsoftware.interfaces.objects.js';

export interface IRequest_Client_ConsentSoftwareServer_GetDomainSettings
  extends plugins.trInterfaces.implementsTR<
    plugins.trInterfaces.ITypedRequest,
    IRequest_Client_ConsentSoftwareServer_GetDomainSettings
  > {
  method: 'getDomainSettings';
  request: {
    domain: string;
  };
  response: {
    domain: string;
    consentTuples: IConsentTuple[];
  };
}
